import json
import logging
import sys
import pathlib
from PyInquirer import prompt, print_json

logging.basicConfig(
    filename='fix_log.log',
    level=logging.INFO,
)

DEFAULT_LOG_FOLDER = pathlib.Path('.') / 'logs'
_CONFIG = pathlib.Path('config.json')


def fix(log, malformed):
    handled, q = 0, []
    log_folder = log.parent
    # The malformed line should be joined into the previous line.
    error_index = set( i - 1 for i in malformed.keys()) 
    with open(log, 'r') as raw, open(log_folder/f'{log.stem}-fixed.txt', 'w') as fixed:
        print('Start to fix log...')
        for index, line in enumerate(raw):
            if index in error_index:
                q += [line.rstrip()]
            else:
                if not q:
                    fixed.write(line)
                else:
                    q += [line]
                    fixed.write(''.join(q))
                    q.clear()
                    handled += 1
        logging.info(f"Total {index} lines, fixed {handled} lines.")


def analysis(log):
    """ Find out malformed lines. """
    logging.info(f'Processing Log {log.resolve()}')
    malformed, messed = dict(), 0
    with open(log, 'r') as raw:
        print('Analysising log...')
        for index, line in enumerate(raw):
            #
            # According to https://wow.gamepedia.com/COMBAT_LOG_EVENT,
            # log format should be:
            # timestamp  log,information,seperate,with,coma
            #
            # which, printed in an actual log file becomes:
            # 6/2 21:14:01.376  SPELL_CAST_SUCCESS,Player-963-04C42E5B,"Wxyz-暗影之月",...
            #
            # and a malformed line doesn't follow the format, e.g:
            # ",0xa28,0x0,282515,"彼世形體",0x20
            #
            # so use 2 spaces to check if this line is broken.
            #
            raw = line.split('  ')
            if len(raw) < 2:
                messed += 1
                malformed[index] = line
    logging.info(f'Found total {messed} malformed lines.')
    return malformed


def inquiry():
    """ Ask for log file. """
    default_path = [str(DEFAULT_LOG_FOLDER)]
    if _CONFIG.exists():
        with open(_CONFIG, 'r') as f:
            try:
                config = json.load(f)
                default_path += [config['path']]
            except:
                pass
    default_path += ['Others']

    which_path = [
        {
            'type': 'list',
            'message': 'Enter the path of log folder: ',
            'name': 'path',
            'choices': default_path,
        },
        {
            'type': 'input',
            'message': 'Please enter the path: ',
            'name': 'path',
            'when': lambda answers: answers['path'] == 'Others'
        }
    ]

    log_folder = prompt(which_path)['path']
    log_folder = pathlib.Path(log_folder)

    if not log_folder.is_dir():
        raise ValueError('Not a valid folder')
    # Save path to config.
    with open(_CONFIG, 'w') as f:
        json.dump({'path': str(log_folder)}, f)

    logs = [log for log in log_folder.iterdir() if 'WoWCombatLog' in log.name]
    if not logs:
        print('No file named WoWCombatLog. List all files.')
        logs = [log for log in log_folder.iterdir()]

    which_log = [
        {   
            'type': 'list',
            'message': 'Select a log:',
            'name': 'file',
            'choices': [
                log.name for log in logs
             ],
        },
    ]

    answer = prompt(which_log)
    return log_folder / answer['file']


def build_config(log):
    """ Record used path. """
    config = {'path': str(log.parent)}
    with open(_CONFIG, 'w') as f:
        json.dump(config, f)


def main():
    try:
        log = inquiry()
    except ValueError as e:
        print(e)
        sys.exit()
    malformed = analysis(log)
    fix(log, malformed)
    print('Done!')

if __name__ == '__main__':
    main()

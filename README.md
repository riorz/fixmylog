## Description
Tiny program to fix my combat logs.  
Visage from beyond is beyond recognition under zh_TW client.   

Blizzard快點修好log好不好。

## Usage
```bash
python3 -m venv venv
. ./venv/bin/activate
pip install -r requirements.txt
python fixer.py
 ```  

### If you are using the [Warcraftlogs](https://www.warcraftlogs.com) site, please note this is not for live log.  
It will generate a WoWCombatLog-fixed.txt, which is (supposedly) fixed.  
### 不適用於live log.  
打完之後跑這個程式, 再選擇Wowcombatlog-fixed.txt檔上傳


import pathlib
import fixer

def test_analysis():
    log = pathlib.Path('./logs/malformed.txt')
    result = fixer.analysis(log)
    assert result == {
            1: '",0xa28,0x0,282515,"彼世形體",0x20\n',
            3: '",0xa48,0x0,0000000000000000,nil,0x80000000,0x80000000,282517,"恐嚇回音",0x20\n',
            7: '\n'
    } 
